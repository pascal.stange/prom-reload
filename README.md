# prom-reload
is a simple service to download prometheus rules from an external source via cURL.

## Usage
The service listens to following variables:
```
TARGET # defines the file target. the file will be renamed to 'alert.rules'
USERNAME # username for basic auth
PASSWORD # password for basic auth
PROM # reload endpoint of prometheus. Usually http://prometheus:9090/-/reload
```

