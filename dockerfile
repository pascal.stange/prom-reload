FROM alpine:3.14

RUN apk add curl

COPY ./script.sh /script.sh

RUN chmod +x ./script.sh

RUN mkdir /prom && touch /prom/alert.rules

CMD ./script.sh