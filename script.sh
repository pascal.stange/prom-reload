#!/bin/sh
touch hash.txt
while true; do
  HASH=$(md5sum $(readlink -f /prom/alert.rules))
  curl -o /prom/alert.rules -u $USERNAME:$PASSWORD $TARGET
  NEW_HASH=$(md5sum $(readlink -f /prom/alert.rules))
  if [ "$HASH" != "$NEW_HASH" ]; then
    echo "[$(date +%s)] Trigger refresh"
    curl -sSL -X POST "$PROM" >/dev/null
  fi
  sleep 6
done
